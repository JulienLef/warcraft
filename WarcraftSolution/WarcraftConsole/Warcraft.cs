﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarcraftConsole
{
    public class Peasant
    {
        public string name;
        public string type;
        public string race;
        public string faction;
        public int hit_point;
        public int armor;

        public Peasant(string name)
        {
            this.name = name;
            this.type = "Peasant";
            this.race = "Human";
            this.faction = "Alliance";
            this.hit_point = 30;
            this.armor = 0;
        }

        public string SayHello()
        {
            return string.Format("Salut je m'appelle {0} et je suis un {1}.", this.name, this.type);
        }

        public string Grunt()
        {
            return "Hummm";
        }

        public string Talk()
        {
            return "Salut, Que veux-tu Péon ?";
        }

        public string TalkToPeasant(Peasant peasant)
        {
            return string.Format("Tiens un autre Peasant. Salut mon ami, comment te prenomme-tu ? \nMoi ? {0} mon ami.", peasant.name);
        }

        public string TalkToPeon(Peon peon)
        {
            return string.Format("Hey toi ! Dégage de chez moi !\n{0} faire comme il veut !", peon.name);
        }

    }
    public class Peon
    {
        public string name;
        public string type;
        public string race;
        public string faction;
        public int hit_point;
        public int armor;

        public Peon(string name)
        {
            this.name = name;
            this.type = "Peon";
            this.race = "Orc";
            this.faction = "Horde";
            this.hit_point = 30;
            this.armor = 0;
        }

        public string SayHello()
        {
            return string.Format("Moi {0}, moi {1}.", this.name, this.type);
        }

        public string Grunt()
        {
            return "Greuh";
        }

        public string Talk()
        {
            return string.Format("{0} parle pas inconnus.", this.name);
        }

        public string TalkToPeasant(Peasant peasant)
        {
            return "Toi qui ?!\nAhh ! Que l'on me vienne aux secours !";
        }

        public string TalkToPeon(Peon peon)
        {
            return string.Format("Hé ! {0} vouloir ton manger !\nNon ! Ca être manger de {0} !", this.name, peon.name);
        }


    }
}
