﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarcraftConsole
{
    class Program
    {
        static string SetName()
        {
            Console.Write("Donnez lui un nom : ");
            string name = Console.ReadLine();
            return name;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Créer un Peasant :");
            string name = SetName();
            WarcraftConsole.Peasant peasant = new WarcraftConsole.Peasant(name);

            Console.WriteLine("Créer un Peon :");
            name = SetName();
            WarcraftConsole.Peon peon = new WarcraftConsole.Peon(name);

            Console.WriteLine(peasant.SayHello());
            Console.WriteLine(peon.SayHello());
            Console.WriteLine(peon.Grunt());
            Console.WriteLine(peasant.Talk());
            Console.WriteLine(peon.Talk());
            Console.WriteLine(peasant.Grunt());
            Console.WriteLine(peasant.TalkToPeasant(peasant));
            Console.WriteLine(peasant.TalkToPeon(peon));
            Console.WriteLine(peon.TalkToPeasant(peasant));
            Console.WriteLine(peon.TalkToPeon(peon));
            Console.ReadKey();
        }

    }
}
