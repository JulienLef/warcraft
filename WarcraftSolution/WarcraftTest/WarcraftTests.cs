﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using WarcraftConsole;

namespace WarcraftTest
{
    [TestClass]
    public class WarcraftTests
    {
        [TestMethod]
        public void TestPeasantConstructor()
        {
            Peasant peasant = new Peasant("Baptiste");
            Assert.AreEqual(peasant.name, "Baptiste");
            Assert.AreEqual(peasant.type, "Peasant");
            Assert.AreEqual(peasant.race, "Human");
            Assert.AreEqual(peasant.faction, "Alliance");
            Assert.AreEqual(peasant.hit_point, 30);
            Assert.AreEqual(peasant.armor, 0);
        }

        [TestMethod]
        public void TestPeonConstructor()
        {
            Peon peon = new Peon("Pépito");
            Assert.AreEqual(peon.name, "Pépito");
            Assert.AreEqual(peon.type, "Peon");
            Assert.AreEqual(peon.race, "Orc");
            Assert.AreEqual(peon.faction, "Horde");
            Assert.AreEqual(peon.hit_point, 30);
            Assert.AreEqual(peon.armor, 0);
        }

        [TestMethod]
        public void TestPeasantSayHello()
        {
            Peasant peasant = new Peasant("Baptiste");
            Assert.AreEqual(peasant.SayHello(), "Salut je m'appelle Baptiste et je suis un Peasant.");
        }

        [TestMethod]
        public void TestPeontSayHello()
        {
            Peon peon = new Peon("Pépito");
            Assert.AreEqual(peon.SayHello(), "Moi Pépito, moi Peon.");
        }

        [TestMethod]
        public void TestPeasantGrunt()
        {
            Peasant peasant = new Peasant("Baptiste");
            Assert.AreEqual(peasant.Grunt(), "Hummm");
        }

        [TestMethod]
        public void TestPeonGrunt()
        {
            Peon peon = new Peon("Pépito");
            Assert.AreEqual(peon.Grunt(), "Greuh");
        }

        [TestMethod]
        public void TestPeasantTalk()
        {
            Peasant peasant = new Peasant("Baptiste");
            Assert.AreEqual(peasant.Talk(), "Salut, Que veux-tu Péon ?");
        }

        [TestMethod]
        public void TestPeonTalk()
        {
            Peon peon = new Peon("Pépito");
            Assert.AreEqual(peon.Talk(), "Pépito parle pas inconnus.");

        }

        [TestMethod]
        public void TestPeasantTalkToPeasant()
        {
            Peasant peasant = new Peasant("Baptiste");
            Assert.AreEqual(peasant.TalkToPeasant(peasant), "Tiens un autre Peasant. Salut mon ami, comment te prenomme-tu ? \nMoi ? Baptiste mon ami.");
        }

        [TestMethod]
        public void TestPeonTalkToPeasant()
        {
            Peasant peasant = new Peasant("Baptiste");
            Peon peon = new Peon("Pépito");
            Assert.AreEqual(peon.TalkToPeasant(peasant), "Toi qui ?!\nAhh ! Que l'on me vienne aux secours !");
        }

        [TestMethod]
        public void TestPeasantTalkToPeon()
        {
            Peasant peasant = new Peasant("Baptiste");
            Peon peon = new Peon("Pépito");
            Assert.AreEqual(peasant.TalkToPeon(peon), "Hey toi ! Dégage de chez moi !\nPépito faire comme il veut !");
        }

        [TestMethod]
        public void TestPeonTalkToPeon()
        {
            Peon peon = new Peon("Pépito");
            Assert.AreEqual(peon.TalkToPeon(peon), "Hé ! Pépito vouloir ton manger !\nNon ! Ca être manger de Pépito !");
        }

    }
}
